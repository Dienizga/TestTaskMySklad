package com.example.customwarehousetask.api.json;

import lombok.Data;

@Data
public class ReportRequest {
    private String nameProduct;
    private String nameWarehouse;
}
